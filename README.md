VRC gitCommitterの情報集約リポジトリです。Wikiページのみの運用プロジェクトです

 **Wikiのトップは[こちら](https://gitlab.com/vrc-gitcommitter/wiki/wikis/home)から。** wikiの編集方法もこちらから。

ライセンスは[MIT](https://ja.wikipedia.org/wiki/MIT_License)で公開されています。ライセンスファイルは[LICENSE](LICENSE)。
